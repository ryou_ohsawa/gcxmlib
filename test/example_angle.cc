/**
 * @file example_angle.cc
 * @brief functional test of the angle class
 * @author Ryou Ohsawa
 * @year 2021
 */
#include "gcxmlib.h"
#include <random>

using gcxmlib::angle;
using namespace gcxmlib::literals;


template<class T>
inline void
display_angle(const T angle)
{
  printf("[radian: %.2lf, degree: %.2lf, arcmin: %.2lf, arcsec: %.2lf]\n",
         angle.radian, angle.degree, angle.arcmin, angle.arcsec);
}

inline void
put_boolean(const bool b)
{
  if (b==true)
    printf("true\n");
  else
    printf("false\n");
}


int
main(int argn, char** argv)
{
  auto s = std::chrono::system_clock::now();

  printf("The domain of the `angle` instance is [0, 2pi).\n");
  printf("Create `angle` instances with various angles.\n");
  printf("# angle(1.0)\n  ");
  display_angle(angle(1.0));
  printf("# angle(1.5)\n  ");
  display_angle(angle(1.5));

  printf("\n");
  printf("Angle is automatically wrapped within [0, 2pi).\n");
  printf("# angle(10.0)\n  ");
  display_angle(angle(10.0));
  printf("# angle(-3.0)\n  ");
  display_angle(angle(-3.0));
  printf("# angle(2M_PI)\n  ");
  display_angle(angle(2*M_PI));

  printf("\n");
  printf("Cast to (double) returns radian.\n");
  printf("(double)angle(1.0) = %lf\n",
         (double)angle(1.0));
  printf("static_cast<double>(angle(1.0)) = %lf\n",
         static_cast<double>(angle(1.0)));
  double tmp = angle(1.0);
  printf("double tmp = angle(1.0); tmp = %lf\n", tmp);

  printf("\n");
  printf("Arithmetic operations with the `angle` and float values.\n");
  printf("# - angle(1.0)\n  ");
  { auto x = -angle(1.0); display_angle(x); }
  printf("# 1.5 + angle(1.0)\n  ");
  { auto x = 1.5 + angle(1.0); display_angle(x); }
  printf("# angle(1.0) + 0.5\n  ");
  { auto x = angle(1.0) + 0.5; display_angle(x); }
  printf("# 1.5 - angle(1.0)\n  ");
  { auto x = 1.5 - angle(1.0); display_angle(x); }
  printf("# angle(1.0) - 0.5\n  ");
  { auto x = angle(1.0) - 0.5; display_angle(x); }
  printf("# 1.5 * angle(1.0)\n  ");
  { auto x = 1.5 * angle(1.0); display_angle(x); }
  printf("# angle(1.0) * 0.5\n  ");
  { auto x = angle(1.0) * 0.5; display_angle(x); }
  printf("# 1.5 / angle(1.0)\n  ");
  { auto x = 1.5 / angle(1.0); display_angle(x); }
  printf("# angle(1.0) / 0.5\n  ");
  { auto x = angle(1.0) / 0.5; display_angle(x); }

  printf("\n");
  printf("Arithmetic operations between the `angle` instances.\n");
  printf("# angle(1.0) + angle(1.5)\n  ");
  { auto x = angle(1.0) + angle(1.5); display_angle(x); }
  printf("# angle(1.0) - angle(0.5)\n  ");
  { auto x = angle(1.0) - angle(0.5); display_angle(x); }

  printf("\n");
  printf("Compare two `angle` instances.\n");
  printf("# angle(2.0) < angle(1.0)\n  ");
  put_boolean(angle(2.0) < angle(1.0));
  printf("# angle(2.0) > angle(1.0)\n  ");
  put_boolean(angle(2.0) > angle(1.0));
  printf("# angle(2.0) == angle(2.0)\n  ");
  put_boolean(angle(2.0) == angle(2.0));

  printf("\n");
  printf("Use literals to construct `angle` instances.\n");
  printf("# 123.45deg\n");
  { auto x = 123.45deg; display_angle(x); }
  printf("# 123.45amin\n");
  { auto x = 123.45amin; display_angle(x); }
  printf("# 123.45asec\n");
  { auto x = 123.45asec; display_angle(x); }

  auto e = std::chrono::system_clock::now();
  std::chrono::duration<double> dt = e-s;
  printf("\n# elapsed time:: %.8lf ms\n", dt.count()*1e3);

  return 0;
}
