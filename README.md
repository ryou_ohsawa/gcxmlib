# GCXMLib
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Documentation Status](https://readthedocs.org/projects/gcxmlib/badge/?version=latest)](https://gcxmlib.readthedocs.io/en/latest/?badge=latest)

## Overview
This module provides some functions to manipulate tracklets on the unit sphere.



## Dependencies

``` python
numpy
astropy
matplotlib
```

## References
