# Index of the C++ reference


## Constants and Aliases

- [Constants related to angles](../constants/#constants-related-to-angles)
    - `radian_to_degree`
    - `degree_to_radian`
    - `radian_to_arcmin`
    - `arcmin_to_radian`
    - `radian_to_arcsec`
    - `arcsec_to_radian`
    - `literals::rad`
    - `literals::deg`
    - `literals::amin`
    - `literals::asec`
- [Constants related to time](../constants/#constants-related-to-time)
    - `sec_t`
    - `default_clock`
    - `timestamp_t`
- [Miscellaneous Constants](../constants/#miscellaneous-constants)
    - `__debug__`
    - `__epsilon__`
    - `__exact_zero__`


## Classes

- [`angle`](../angle/):
- [`vector3`](../vector3/):
- [`matrix3`](../matrix3/):
- [`direction_cosine`](../direction_cosine/):
- [`footprint`](../footprint/):
- [`great_circle`](../great_circle/):
- [`minor_arc`](../minor_arc/):
- [`trail`](../trail/):
- [`trajectory`](../trajectory/):


## Functions

- [`now()`](../functions/#functions-associated-with-timestamp):
- [`generate_timestamp()`](../functions/#functions-associated-with-timestamp):
- [`timestamp_to_string()`](../functions/#functions-associated-with-timestamp):
- [`advance_timestamp()`](../functions/#functions-associated-with-timestamp):
- [`radian()`](../functions/#functions-associated-with-angle):
- [`degree()`](../functions/#functions-associated-with-angle):
- [`arcmin()`](../functions/#functions-associated-with-angle):
- [`arcsec()`](../functions/#functions-associated-with-angle):
- [`rotation_matrix_x()`](../functions/#functions-associated-with-matrix):
- [`rotation_matrix_y()`](../functions/#functions-associated-with-matrix):
- [`rotation_matrix_z()`](../functions/#functions-associated-with-matrix):
- [`solve_chol3()`](../functions/#functions-associated-with-matrix):
- [`eigen_pow()`](../functions/#functions-associated-with-matrix):
